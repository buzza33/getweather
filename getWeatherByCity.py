import sys
import urllib, json
import tkMessageBox
import Tkinter as tk

class WeatherApp(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.entry = tk.Entry(self)
        self.button = tk.Button(self, text="Get", command=self.on_button)
        self.button.pack()
        self.entry.pack()
        self.insert = tk.INSERT
        self.text = tk.Text(self)
        self.text.pack()
        self.apikey = "e6274b00e205ff89dfbc70b416b6ae30"

    def on_button(self):
        self.text.insert(self.insert, self.getWeatherInYourCity())

    def getWeatherInYourCity(self):
		url = "http://api.openweathermap.org/data/2.5/weather?q=" + self.entry.get() + "&appid=" + self.apikey + "&units=metric"
		response = urllib.urlopen(url)
		data = json.loads(response.read())
		return data['name'] + ', ' + data['sys']['country'] + ': ' + str(data['main']['temp']) + "\n"

app = WeatherApp()
app.mainloop()